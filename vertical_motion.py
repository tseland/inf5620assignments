from numpy import *
from matplotlib.pyplot import *
import sys

import nose.tools as nt

def get_values():
	g = 9.81			#gravity
	rho = 1000			#density of water
	r = 0.11			#radius
	V = 4/3.*pi*r**3		#Volume
	mu_k = 1.004*10**(-6)		#kinetic viscosity 20 degrees
	mu_d = rho*mu_k			#dynamic viscosity
	d = 2*r				#diameter
	m = 0.43			#mass of ball
	rho_b =	m/V			#density of ball
	A = pi*r**2			#cross-sectional area
	C_d = 0.45			#drag coefficient ball

	T = 3				#total time
	dt = 0.01			#step length
	I = 0				#initial condition


	
	return T ,dt, I, g, rho, V, mu_d, d, rho_b, A, C_d


def solver(T ,dt, I, g, rho, V, mu, d, rho_b, A, C_d, b_s, b_q, a_s, a_q):
	Nt = int(T/dt)
	T = Nt*dt
	u = zeros(Nt+1)
	t = linspace(0, T, Nt+1)

	u[0] = I

	if(b_s == "b_1"):
		def b_s(t):
			return g*((rho/rho_b)-1)
		def b_q(t):
			return g*((rho/rho_b)-1)
		def a_s(t): 
			return (3*pi*d*mu)/(rho_b*V)
		def a_q(t):
			return 0.5*C_d*(rho*A)/(rho_b*V)
		
	for n in range(0, Nt):	
		if(mu != 0):
			Re = g*d*abs(u[n])/mu
		else:
			Re = 0.5	


		if(Re < 1): #Choose Stokes
			u[n+1] = ((1-a_s(t[n])*0.5*dt)*u[n]+dt*0.5*(b_s(t[n+1])+b_s(t[n])))/(1+a_s(t[n+1])*0.5*dt)
		else:	#Quadratic 
			u[n+1] = (b_q(t[n]+dt/2)*dt+u[n])/(1+a_q(t[n]+dt/2)*abs(u[n])*dt)

	
			 
	

	return u,t

def read_command_line(use_argparse=True):
	if use_argparse:
		parser = define_command_line_options()
		args = parser.parse_args()
		print 'T={}, dt={}, I_u={},I_z={},g={},rho={},\nmu={},r={},m={},C_d={}'.format(
			args.T, args.dt,args.I_u,args.I_z,args.g,args.rho,args.mu,args.r,args.m,args.C_d)
		return args.T, args.dt,args.I_u,args.I_z,args.g,args.rho,args.mu,args.r,args.m,args.C_d
	else:
		if len(sys.argv) < 2:
			print 'Usage: %s I a on/off dt1 dt2 dt3 ...' % \
				sys.argv[0]; sys.exit(1)
		T = float(sys.argv[1])
		dt = float(sys.argv[2])
		I_u = float(sys.argv[3])
		I_z = float(sys.argv[4])
		g = float(sys.argv[5])
		rho = float(sys.argv[6])
		mu = float(sys.argv[7])
		r = float(sys.argv[8])
		m = float(sys.argv[9])
		C_d = float(sys.argv[10])
		

	
		return T,dt,I_u,I_z,g,rho,mu,r,m,C_d

def define_command_line_options():
		import argparse
		parser = argparse.ArgumentParser()
		parser.add_argument('--T', '--stop_time', type=float,
					default=1.0, help='end time of simulation',
					metavar='T')
		parser.add_argument('--dt', '--time_step_values', type=float,
					default=1.0, help='time step values',
					metavar='dt')
		parser.add_argument('--I_u', '--initial_condition_speed', type=float,
					default=0.0, help='speed at the start, u(0)',
					metavar='I_u')
		parser.add_argument('--I_z', '--initial_condition_height', type=float,
					default=-5.0, help='position at the start, z(0)',
					metavar='I_z')
		parser.add_argument('--g', '--gravity', type=float,
					default=9.81, help='gravity',
					metavar='g')
		parser.add_argument('--rho', '--density of fluid', type=float,
					default=1.2, help='density of fluid',
					metavar='rho')
		parser.add_argument('--mu', '--viscocity', type=float,
					default=1.0, help='viscocity',
					metavar='mu')
		parser.add_argument('--r', '--radius', type=float,
					default=1.0, help='radius of the element',
					metavar='d')
		parser.add_argument('--m', '--mass', type=float,
					default=1.0, help='mass of element',
					metavar='m')
		parser.add_argument('--C_d', '--drag_coefficient', type=float,
					default=1.0, help='drag coefficient of element',
					metavar='C_d')
		
		return parser


def test_stokes():	
	T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d = get_values()
	b = "b_1"
	a_s = 1
	a_q = 1

	result_u,t = solver(T, dt, I, g, rho, V, mu_d, d, rho_b, A, C_d, b, b, a_s, a_q)

	a = (3*pi*d*mu_d)/(rho_b*V)
	b = g*((rho/rho_b)-1)
	result_test = ((1-a*0.5*dt)*I+b*dt)/(1+a*0.5*dt)	

	nt.assert_equal(result_u[1],result_test)


def test_exact_solution():
	T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d = get_values()	
	rho = 0				#density of water
	mu_k = 1.004*10**(-6)		#kinetic viscosity 20 degrees
	mu_d = rho*mu_k			#dynamic viscosity
	
	b = "b_1"
	a_s = 1
	a_q = 1

	result_u,t = solver(T, dt, I, g, rho, V, mu_d, d, rho_b, A, C_d, b, b, a_s, a_q)

	
	Nt = int(T/dt)
	T = Nt*dt
	result_test = zeros(Nt+1)
	
	for n in range(0, Nt+1):
		result_test[n] = I-g*dt*n
	
	diff = np.abs(result_u-result_test).max()
	nt.assert_almost_equal(diff, 0, delta=1E-12)



def test_linear_solution():
	
	#Test problem where u=c*t+I is the exact solution, to be
	#reproduced (to machine precision) by any relevant method.
	
	c = -0.5
	
	def exact_solution(t):
		return c*t + I
	def a(t):
		return t**0.5 # can be arbitrary
	def b_s(t):
		return c + a(t)*exact_solution(t)
	def b_q(t):
		return c + a(t)*abs(exact_solution(t-dt/2))*exact_solution(t+dt/2)
	
	
	T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d = get_values()

	u,t = solver(T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d, b_s,b_q, a, a )
	
	u_e = exact_solution(t)
	difference = abs(u_e[1] - u[1]).max() # max deviation
	
	# No of decimal places for comparison depend on size of c
	nt.assert_almost_equal(difference, 0, places=14)

	"""
	plot(t, u,'r')
	hold('on')
	plot(t, u_e, 'b')
	show()
	"""

def test_quadratic_solution():
	#Test problem where u=c*t+I is the exact solution, to be
	#reproduced (to machine precision) by any relevant method.
	
	c = -0.5
	
	def exact_solution(t):
		return c*t**2 + I
	def a(t):
		return t**0.5 # can be arbitrary
	def b_s(t):
		return 2*t*c+a(t)*exact_solution(t) 
	def b_q(t):
		return 2*t*c+a(t)*abs(exact_solution(t))*exact_solution(t) 
		
	T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d = get_values()

	E_values=[]
	dt_values=[0.5, 0.3, 0.1, 0.05, 0.01]
	for dt in dt_values:
		u,t = solver(T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d, b_s,b_q, a, a )
		u_e = exact_solution(t)
		e=u_e - u
		E=sqrt(dt*sum(e**2))
		E_values.append(E)
	m=len(dt_values)
	tol=0.1
	expected_rate= 2
	r=[log(E_values[i-1]/E_values[i])/
                    log(dt_values[i-1]/dt_values[i])
                    for i in range(1, m, 1)]	
	diff=abs(expected_rate- r[-1])
	if diff>tol:
		return False
	return True	


def test_terminal_velocity():
		
	T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d = get_values()

	b = "b_1"
	a_s = 1
	a_q = 1

	u,t = solver(T, dt, I,g, rho, V, mu_d, d, rho_b, A, C_d, b, b, a_s, a_q )

	Nt = int(T/dt)
	T = Nt*dt

	Re = g*d*abs(u[T])/mu_d

	
	def b(t):
		return g*((rho/rho_b)-1)
	def a_s(t): 
		return (3*pi*d*mu_d)/(rho_b*V)
	def a_q(t):
		return 0.5*C_d*(rho*A)/(rho_b*V)

	if Re<1:
		Tv= -a_s(T)*u[Nt]+b(T)
		
	else:
		Tv= -a_q(T)*abs(u[Nt])*u[Nt]+b(T)

	nt.assert_almost_equal(Tv,0, places= 13)



if __name__ == "__main__":
	g = 9.81			#gravity
	rho = 1000			#density of water
	r = 0.11			#radius
	V = 4/3.*pi*r**3		#Volume
	mu = 8.9*10**(-4)		#viscocity
	d = 2*r				#diameter
	m = 0.43			#mass of ball
	rho_b =	m/V			#density of ball
	A = pi*r**2			#cross-sectional area
	C_d = 0.45			#drag coefficient ball

	T = 5				#total time
	dt = 0.001			#step length
	I = 0				#initial condition

	
	b = "b_1"
	a_s = "a_s"
	a_q = "a_q"

	u,t = solver(T, dt, I, g, rho, V, mu, d, rho_b, A, C_d, b , a_s, a_q)
	plot(t,u)
	show()
